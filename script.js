'use strict';

const photo = document.querySelector('.dice');
const happy = document.querySelector('.happy');
const btnNew = document.querySelector('.btn--new');
const btnRoll = document.querySelector('.btn--roll');

const appear = function () {
  photo.classList.remove('hidden');
  happy.classList.remove('hidden');
};
const hidden = function () {
  photo.classList.add('hidden');
  happy.classList.add('hidden');
};

btnRoll.addEventListener('click', appear);
btnNew.addEventListener('click', hidden);
